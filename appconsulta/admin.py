from django.contrib import admin
from .models import Artista, Obra
# Register your models here.

admin.site.register(Artista)
admin.site.register(Obra)
