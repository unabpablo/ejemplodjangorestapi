from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from .models import Artista, Obra
from .serializers import ArtistaSerializer, ObraSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@api_view(["GET"])
def vista_inicio(request):
    content = {"ack": "Bienvenido a nuestro servicio de consulta!"}
    return JsonResponse(content)


@api_view(["GET"])
@csrf_exempt
def listar_obras(request):
    #user = request.user.id
    obras = Obra.objects.all()
    serializer = ObraSerializer(obras, many=True)
    return JsonResponse({'obras': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["GET"])
@csrf_exempt
def listar_pintores(request):
    pintores = Artista.objects.all()
    serializer = ArtistaSerializer(pintores, many=True)
    return JsonResponse({'artista': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["POST"])
@csrf_exempt
def agregar_obra(request):
    serializer = ObraSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PUT"])
@csrf_exempt
def actualizar_obra(request, obra_id):
    serializer = ObraSerializer(data=request.data)
    if serializer.is_valid():
        try:
            obra_item = Obra.objects.get(id=obra_id)
            obra_item.titulo = serializer.validated_data['titulo']
            obra_item.descripcion = serializer.validated_data['descripcion']
            obra_item.artista = serializer.validated_data['artista']
            obra_item.save()
            obra = Obra.objects.get(id=obra_id)
            serializer = ObraSerializer(obra)
            return JsonResponse({'obra': serializer.data}, safe=False, status=status.HTTP_200_OK)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return JsonResponse({'error': 'Error interno...'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["DELETE"])
@csrf_exempt
def borrar_obra(request, obra_id):
    #user = request.user.id
    try:
        obra = Obra.objects.get(id=obra_id)
        obra.delete()
        return JsonResponse({'obra eliminada': obra_id}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Something went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
