from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.vista_inicio),
    path('listarobras/', views.listar_obras),
    path('listarpintores/', views.listar_pintores),
    path('agregarobra/', views.agregar_obra),
    path('actualizarobra/<int:obra_id>', views.actualizar_obra),
    path('borrarobra/<int:obra_id>', views.borrar_obra)
]
