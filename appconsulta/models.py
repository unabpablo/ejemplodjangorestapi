from django.db import models
from django.conf import settings
from django.utils import timezone


class Artista(models.Model):
  nombre = models.CharField(max_length=200)
  pseudonimo = models.CharField(max_length=100)
  genero = models.CharField(max_length=200)
  fecha_registro = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.nombre


class Obra(models.Model):
  titulo = models.CharField(max_length=200)
  descripcion = models.CharField(max_length=300)
  artista = models.ForeignKey(Artista, on_delete=models.CASCADE)
  fecha_ingreso = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.titulo
