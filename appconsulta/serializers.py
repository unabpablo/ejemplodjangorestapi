from rest_framework import serializers
from .models import Artista, Obra


class ArtistaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artista
        fields = ['id', 'nombre', 'pseudonimo', 'genero', 'fecha_registro']


class ObraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Obra
        fields = ['id', 'titulo', 'descripcion', 'artista', 'fecha_ingreso']
